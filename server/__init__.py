"""
.. highlight:: python

Server module provides a simple HTTP server for development-time
purposes.

Usage::

  import server
  my_server = new server.MySuperServer()
  my_server.start()


"""
# NOTE! This is not really functional server, only an example for docstrings
import os

class MySuperServer(object):
  """docstring for MySuperServer"""
  def __init__(self, arg):
    super(MySuperServer, self).__init__()
    self.arg = arg

  def get_users():
    """Implements the REST get users"""
    """
    @api {get} /users/ Returns list of users
    @apiName GetUsers
    @apiGroup Users
    @apiVersion 0.1.0
    @apiDescription
    Returns all the active and inactive users
    from the system.
    @apiExample {json} Example return

    [{ "id": 1, "name": "John"}, {}]
    """
    # TODO: Implement

  def get_user(id):
    """Implements the REST get user"""
    """
    @api {get} /users/:id Returns selected user
    @apiName GetUser
    @apiGroup Users
    @apiVersion 0.2.0
    @apiDescription
    Returns the user based on id
    from the system. If not found, returns 404
    @apiExample {json} Example return

    { "id": 1, "name": "John"}
    """
    # TODO: Implement

  def start(port=8000):
      """
      Runs the server
      """
      server_address = ('', port)
      httpd = server_class(server_address, handler_class)
      httpd.serve_forever()