.. _server:

Server
======
This section provides you a deeper understanding into server logic and architecture.

.. contents::
    :local:

Python API
----------

.. automodule:: server
   :members:

REST API
--------

`See separate REST API <http://localhost/api/>`_ instead.

Data flow
---------

Extension points
