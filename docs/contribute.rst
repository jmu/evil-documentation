Contributing
============
Your contribution is very welcome! However, when sending code patches, ensure
following guidelines are adhered:

- Code can be merged against latest code develop
- Tests completes without any errors
- Changes are covered with tests

See also :ref:`develop`