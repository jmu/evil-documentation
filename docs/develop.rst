.. _develop:

Development
===========
This page gets you started with project development.

Make sure you :ref:`understand the server architecture <server>` first.