.. highlight:: bash

Evil Documentation!
===================
This document is created only for demostration purposes.

.. figure:: http://i.huffpost.com/gen/238474/HTML5-LOGO.jpg
   :figwidth: 50%

How about some code?

.. code-block:: html

    <html>
      <body id="content"></body>
    </html>

.. literalinclude:: client/app.js
   :language: javascript
   :lines: 1-5


Installation steps:

#.  Install requirements::

      apt-get install nodejs
      sudo npm install -g gulp bower
      npm install

    .. note:: Requires admin rights

      In order to install packages globally you need admin (in Linux: root)
      level permissions.

#.  Start app::

      node app.js


Future reading:

.. toctree::
   :maxdepth: 2

   docs/contribute
   docs/develop
   docs/server




